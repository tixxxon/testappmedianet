import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

axios.defaults.baseURL = "http://localhost:3000";

let lsLikePosts = localStorage.getItem("likePosts");
if (!lsLikePosts) {
  lsLikePosts = [];
  localStorage.setItem("likePosts", JSON.stringify(lsLikePosts));
} else {
  lsLikePosts = JSON.parse(lsLikePosts);
}

export default new Vuex.Store({
  state: { likePosts: lsLikePosts },
  mutations: {
    LIKE_POST(storage, postId) {
      let postIdx = storage.likePosts.indexOf(postId);
      if (postIdx != -1) {
        storage.likePosts.splice(postIdx, 1);
      } else {
        storage.likePosts.push(postId);
      }

      localStorage.setItem("likePosts", JSON.stringify(storage.likePosts));
    },
  },
  actions: {
    async likePost({ commit }, likeData) {
      await axios.patch(`/posts/${likeData.id}`, {
        like: likeData.count,
      });
      commit("LIKE_POST", likeData.id);
    },
    async dislikePost({ commit }, likeData) {
      await axios.patch(`/posts/${likeData.id}`, {
        like: likeData.count,
      });
      commit("LIKE_POST", likeData.id);
    },
  },
  getters: {
    isLikePost: (state) => (id) => {
      return state.likePosts.some((pId) => pId === id);
    },
  },
  modules: {},
});
