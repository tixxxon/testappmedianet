import Vue from "vue";
import axios from "axios";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "./assets/styles/vendor/flexboxgrid.min.css";

Vue.config.productionTip = false;

axios.defaults.baseURL = "http://localhost:3000";

Vue.prototype.$publicPath = process.env.BASE_URL;
Vue.prototype.$http = axios;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
